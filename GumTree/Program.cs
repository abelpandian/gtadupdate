﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Collections.ObjectModel;

namespace GumTree
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean completed = false;
            try
            {

                string expCnt = "";
                string siteurl = "https://www.gumtree.com.au";
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.AddUserProfilePreference("profile.default_content_setting_values.images", 2);
                chromeOptions.AddArguments("--disable-plugins-discovery");
                chromeOptions.AddArgument("--disable-extensions");
                chromeOptions.AddArgument("--dns-prefetch-disable");
                chromeOptions.AddArgument("--disable-impl-side-painting");
                //chromeOptions.AddArgument("--headless");
                var chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();

                using (var driver = new ChromeDriver(chromeDriverService, chromeOptions, new TimeSpan(0, 3, 0)))
                {
                    driver.Manage().Window.Maximize();
                    driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
                    driver.Navigate().GoToUrl(siteurl + "//m-my-ads.html?show=EXPIRED");

                    IWebElement userid = driver.FindElementById("login-email");
                    userid.SendKeys(ConfigurationManager.AppSettings["username"]);

                    IWebElement pwd = driver.FindElementById("login-password");
                    pwd.SendKeys(ConfigurationManager.AppSettings["password"]);

                    IWebElement submit = driver.FindElementById("btn-submit-login");
                    submit.Click();

                    string bulk = ConfigurationManager.AppSettings["bulk"];
                    Thread.Sleep(500);
                    int idx = 1;
                    bool IsBulkUpdate = bulk.Equals("true");
                    do
                    {
                        var expireCnt = document.DocumentNode.SelectSingleNode("//span[@class='addl-ops-lnks c-hide-mobile']//strong");
                        if (expireCnt != null)
                        {
                            Console.WriteLine(expireCnt.InnerText);
                            expCnt = expireCnt.InnerText.Replace("Expired Ads (", "").Replace(")", "").Trim();
                            if (expCnt == "0")
                            {
                                completed = true;
                                break;
                            }
                                
                        }

                        if ( IsBulkUpdate )
                        {
                            document.LoadHtml(driver.PageSource.Replace("  ", "").Replace("\t", "").Replace(Environment.NewLine, "").Replace("&amp;", "&"));

                            IWebElement blkRepost = driver.FindElement(By.XPath("//button[contains(.,'Repost expired')]"));
                            blkRepost.Click();

                            Thread.Sleep(1000);

                            IWebElement selectAll = driver.FindElement(By.XPath("//button[contains(.,'Select all')]"));
                            selectAll.Click();

                            Thread.Sleep(1000);

                            IWebElement repostBtn = driver.FindElement(By.XPath("//button[contains(.,'Repost')]"));
                            repostBtn.Click();

                            Thread.Sleep(1000);

                            IWebElement skipBtn = driver.FindElement(By.XPath("//button[contains(.,'Skip')]"));
                            skipBtn.Click();

                            Console.WriteLine(idx + " x 20 ads renewed");
                            idx++;
                        }
                        
                        else
                        {
                            
                            ReadOnlyCollection<IWebElement> links = driver.FindElementsByLinkText("Repost without Bump Up");
                            HtmlNodeCollection repostLinks = document.DocumentNode.SelectNodes("//a[@class='repost-ad-free']");
                            /*if (repostLinks != null)
                            {
                                foreach (HtmlNode link in repostLinks)
                                {
                                    Console.WriteLine(siteurl + link.Attributes["href"].Value);
                                    driver.Navigate().GoToUrl(siteurl + link.Attributes["href"].Value);
                                    Thread.Sleep(100);
                                }
                            }*/

                            if (links != null)
                            {
                                int n = links.Count;
                                Collection<string> expLinks = new Collection<string>();

                                for (int i = 0; i < n; i++)
                                {
                                    string val = links[i].GetAttribute("href");
                                    expLinks.Add(val);
                                }
                                for (int i = 0; i < n; i++)
                                {
                                    Console.WriteLine(expLinks[i]);
                                    driver.Navigate().GoToUrl(expLinks[i]);
                                }

                            }
                        }

                        driver.Navigate().GoToUrl(siteurl + "//m-my-ads.html?show=EXPIRED");
                    }
                    while (!completed);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                foreach (var process in Process.GetProcesses())
                    if (process.ProcessName == "chromedriver")
                        process.Kill();
            }
        }
    }
}
